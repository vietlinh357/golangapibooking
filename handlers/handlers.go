package handlers

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strings"

	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"

	"github.com/fumetsunoyami/ban_ve_may_bay/models"
	"github.com/fumetsunoyami/ban_ve_may_bay/repositories"
	"golang.org/x/crypto/bcrypt"
)

func RegisterHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	// 1. Giải mã JSON từ request body
	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// 2. Băm mật khẩu
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.PasswordHash), bcrypt.DefaultCost)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user.PasswordHash = string(hashedPassword)

	// 3. Kiểm tra xem email đã tồn tại chưa
	userRepository := repositories.NewPostgresUserRepository(db)
	existingUser, err := userRepository.GetUserByEmail(user.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if existingUser != nil {
		http.Error(w, "Email đã được sử dụng", http.StatusConflict) // Trả về mã lỗi 409 Conflict
		return
	}

	// 4. Lưu người dùng vào database (chỉ khi email chưa tồn tại)
	if err := userRepository.CreateUser(&user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// 5. Trả về phản hồi
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(map[string]string{"message": "Đăng ký thành công"})
}
func LoginHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	// 1. Giải mã JSON từ request body
	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// 2. Kiểm tra thông tin đăng nhập trong database (sử dụng UserRepository)
	userRepository := repositories.NewPostgresUserRepository(db)
	existingUser, err := userRepository.GetUserByEmail(user.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if existingUser == nil {
		http.Error(w, "Thông tin đăng nhập không chính xác", http.StatusUnauthorized)
		return
	}

	// So sánh mật khẩu đã băm
	if err := bcrypt.CompareHashAndPassword([]byte(existingUser.PasswordHash), []byte(user.PasswordHash)); err != nil {
		http.Error(w, "Thông tin đăng nhập không chính xác", http.StatusUnauthorized)
		return
	}

	// 3. Nếu đăng nhập thành công, tạo và trả về JWT token
	// Tạo token JWT
	tokenString, err := generateJWT(*existingUser)
	if err != nil {
		http.Error(w, "Lỗi tạo token", http.StatusInternalServerError)
		return
	}

	// Trả về token và user ID cho client
	response := map[string]interface{}{
		"token":   tokenString,
		"user_id": existingUser.ID,
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

func generateJWT(user models.User) (string, error) {
	// Đặt thời gian sống của token (ví dụ: 30 phút)
	expirationTime := time.Now().Add(30 * time.Minute)

	// Tạo claims (thông tin về người dùng được mã hóa trong token)
	claims := &jwt.StandardClaims{
		ExpiresAt: expirationTime.Unix(),
		Subject:   fmt.Sprintf("%d", user.ID), // ID của người dùng
		Id:        fmt.Sprintf("%d", user.ID), // Thêm user_id vào claims
	}

	// Tạo token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Ký token bằng một khóa bí mật (secret key)
	tokenString, err := token.SignedString([]byte("your_secret_key")) // Thay bằng khóa bí mật của bạn
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func GetFlightsHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	// Xác thực token JWT
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		http.Error(w, "Token không được cung cấp", http.StatusUnauthorized)
		return
	}
	tokenString := strings.TrimPrefix(authHeader, "Bearer ")

	// Kiểm tra token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte("your_secret_key"), nil // Thay thế bằng khóa bí mật của bạn
	})
	if err != nil || !token.Valid {
		http.Error(w, "Token không hợp lệ", http.StatusUnauthorized)
		return
	}

	// Lấy các tham số từ query string (chỉ origin, destination, departureDate)
	origin := r.URL.Query().Get("origin")
	destination := r.URL.Query().Get("destination")
	departureDateStr := r.URL.Query().Get("departure_date")

	// Parse departureDate (nếu có)
	var departureDate *time.Time
	if departureDateStr != "" {
		parsedDepartureDate, err := time.Parse("2006-01-02", departureDateStr)
		if err != nil {
			http.Error(w, "Invalid departure_date format", http.StatusBadRequest)
			return
		}
		departureDate = &parsedDepartureDate
	}

	// Tạo FlightRepository
	flightRepository := repositories.NewPostgresFlightRepository(db)

	// Gọi hàm tìm kiếm chuyến bay (chỉ truyền các tham số cần thiết)
	flights, err := flightRepository.SearchFlights(origin, destination, departureDate)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Trả về kết quả
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(flights)
}
func CreateBookingHandler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	// 1. Xác thực token JWT
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		http.Error(w, "Token không được cung cấp", http.StatusUnauthorized)
		return
	}
	tokenString := strings.TrimPrefix(authHeader, "Bearer ")

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte("your_secret_key"), nil // Thay thế bằng khóa bí mật của bạn
	})
	if err != nil || !token.Valid {
		http.Error(w, "Token không hợp lệ", http.StatusUnauthorized)
		return
	}

	/*claims, ok := token.Claims.(jwt.MapClaims) //phần lấy user_id
	if !ok {
		http.Error(w, "Token không hợp lệ", http.StatusUnauthorized)
		return
	}

	userIDFloat64, ok := claims["id"].(float64) // Lấy userID từ token
	if !ok {
		http.Error(w, "không tìm được user_id", http.StatusUnauthorized)
		return
	}
	userID := int(userIDFloat64)*/

	// 2. Giải mã JSON từ request body để lấy flightID
	var bookingRequest struct {
		FlightID int `json:"flight_id"`
		UserID   int `json:"user_id"` // Thêm trường userID vào request body
	}
	if err := json.NewDecoder(r.Body).Decode(&bookingRequest); err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	// 3. Kiểm tra flightID
	if bookingRequest.FlightID <= 0 {
		http.Error(w, "Invalid flight_id", http.StatusBadRequest)
		return
	}

	// 4. Kiểm tra tính khả dụng của chuyến bay
	flightRepository := repositories.NewPostgresFlightRepository(db)
	_, err = flightRepository.GetFlightByID(bookingRequest.FlightID) // Chỉ cần kiểm tra tồn tại, không cần lấy chi tiết
	if err != nil {
		if err == sql.ErrNoRows {
			http.Error(w, "Không tìm thấy chuyến bay", http.StatusNotFound)
		} else {
			http.Error(w, fmt.Sprintf("Lỗi khi truy vấn chuyến bay: %v", err), http.StatusInternalServerError)
		}
		return
	}

	// 5. Tạo booking mới
	booking := models.Booking{
		UserID:   bookingRequest.UserID,
		FlightID: bookingRequest.FlightID,
		Status:   "Booked",
		BookedAt: time.Now(),
	}

	bookingRepository := repositories.NewPostgresBookingRepository(db)
	if err := bookingRepository.CreateBooking(&booking); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// 6. Trả về phản hồi
	response := map[string]interface{}{
		"message": "Đặt vé thành công",
		"booking": booking,
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
