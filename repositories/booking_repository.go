package repositories

import (
	"database/sql"
	"fmt"

	"github.com/fumetsunoyami/ban_ve_may_bay/models"
)

// BookingRepository interface định nghĩa các phương thức để tương tác với dữ liệu đặt vé
type BookingRepository interface {
	CreateBooking(booking *models.Booking) error
	GetBookingsByUserID(userID int) ([]models.Booking, error)
}

// postgresBookingRepository là một triển khai của BookingRepository sử dụng PostgreSQL
type postgresBookingRepository struct {
	db *sql.DB
}

// NewPostgresBookingRepository tạo một đối tượng postgresBookingRepository mới
func NewPostgresBookingRepository(db *sql.DB) BookingRepository {
	return &postgresBookingRepository{db}
}

// CreateBooking tạo một booking mới trong cơ sở dữ liệu
func (r *postgresBookingRepository) CreateBooking(booking *models.Booking) error {
	_, err := r.db.Exec("INSERT INTO bookings (user_id, flight_id, status, booked_at) VALUES ($1, $2, $3, $4)",
		booking.UserID, booking.FlightID, booking.Status, booking.BookedAt)
	if err != nil {
		return fmt.Errorf("lỗi khi tạo booking: %v", err)
	}
	return nil
}

// GetBookingsByUserID lấy danh sách các booking của một người dùng
func (r *postgresBookingRepository) GetBookingsByUserID(userID int) ([]models.Booking, error) {
	var bookings []models.Booking

	rows, err := r.db.Query("SELECT * FROM bookings WHERE user_id = $1", userID)
	if err != nil {
		return nil, fmt.Errorf("lỗi khi truy vấn bookings: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var booking models.Booking
		if err := rows.Scan(&booking.ID, &booking.UserID, &booking.FlightID, &booking.Status, &booking.BookedAt); err != nil {
			return nil, fmt.Errorf("lỗi khi quét dữ liệu booking: %v", err)
		}
		bookings = append(bookings, booking)
	}

	return bookings, nil
}
