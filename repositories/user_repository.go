package repositories

import (
	"database/sql"

	"github.com/fumetsunoyami/ban_ve_may_bay/models"
)

type UserRepository interface {
	CreateUser(user *models.User) error
	GetUserByEmail(email string) (*models.User, error)
}
type postgresUserRepository struct {
	db *sql.DB
}

func NewPostgresUserRepository(db *sql.DB) UserRepository {
	return &postgresUserRepository{db}
}

func (r *postgresUserRepository) CreateUser(user *models.User) error {
	_, err := r.db.Exec("INSERT INTO users (email, password_hash) VALUES ($1, $2)", user.Email, user.PasswordHash)
	return err
}

func (r *postgresUserRepository) GetUserByEmail(email string) (*models.User, error) {
	var user models.User
	err := r.db.QueryRow("SELECT id, email, password_hash FROM users WHERE email = $1", email).Scan(&user.ID, &user.Email, &user.PasswordHash)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil // Không tìm thấy người dùng
		}
		return nil, err // Lỗi khác
	}
	return &user, nil
}
