package repositories

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/fumetsunoyami/ban_ve_may_bay/models"
)

// FlightRepository interface định nghĩa các phương thức để tương tác với dữ liệu chuyến bay
type FlightRepository interface {
	// ... các phương thức khác
	SearchFlights(origin, destination string, departureDate *time.Time) ([]models.Flight, error)
	GetFlightByID(id int) (*models.Flight, error)
}
type postgresFlightRepository struct {
	db *sql.DB
}

func NewPostgresFlightRepository(db *sql.DB) FlightRepository {
	return &postgresFlightRepository{db}
}

func (r *postgresFlightRepository) SearchFlights(origin, destination string, departureDate *time.Time) ([]models.Flight, error) {
	// Xây dựng câu truy vấn SQL
	query := `
        SELECT id, flight_number, airline, origin, destination, departure_time, arrival_time, price, aircraft_type
        FROM flights
        WHERE departure_time > NOW()`
	var args []interface{}

	if origin != "" {
		query += " AND origin = $1"
		args = append(args, origin)
	}
	if destination != "" {
		query += " AND destination = $2"
		args = append(args, destination)
	}
	if departureDate != nil {
		query += " AND DATE(departure_time) = $3"
		args = append(args, departureDate)
	}

	// Thực hiện truy vấn
	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, fmt.Errorf("lỗi khi truy vấn cơ sở dữ liệu: %v", err)
	}
	defer rows.Close()

	// Xử lý kết quả
	var flights []models.Flight
	for rows.Next() {
		var flight models.Flight
		if err := rows.Scan(&flight.ID, &flight.FlightNumber, &flight.Airline, &flight.Origin, &flight.Destination, &flight.DepartureTime, &flight.ArrivalTime, &flight.Price, &flight.AircraftType); err != nil {
			return nil, fmt.Errorf("lỗi khi quét dữ liệu: %v", err)
		}
		flights = append(flights, flight)
	}

	return flights, nil
}
func (r *postgresFlightRepository) GetFlightByID(id int) (*models.Flight, error) {
	var flight models.Flight
	err := r.db.QueryRow("SELECT id, flight_number, airline, origin, destination, departure_time, arrival_time, price, aircraft_type FROM flights WHERE id = $1", id).Scan(
		&flight.ID, &flight.FlightNumber, &flight.Airline, &flight.Origin, &flight.Destination,
		&flight.DepartureTime, &flight.ArrivalTime, &flight.Price, &flight.AircraftType,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil // Không tìm thấy chuyến bay
		}
		return nil, fmt.Errorf("lỗi khi truy vấn chuyến bay: %v", err)
	}
	return &flight, nil
}
