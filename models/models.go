package models

import (
	"time"
)

type User struct {
	ID           int    `json:"id"`
	Email        string `json:"email"`
	PasswordHash string `json:"password_hash"`
}

type Flight struct {
	ID            int       `json:"id"`
	Origin        string    `json:"origin"`
	Destination   string    `json:"destination"`
	DepartureTime time.Time `json:"departure_time"`
	ArrivalTime   time.Time `json:"arrival_time"`
	Price         float64   `json:"price"`
	FlightNumber  string    `json:"flight_number"`
	Airline       string    `json:"airline"`
	AircraftType  string    `json:"aircraft_type"`
}

type Seat struct {
	ID         int    `json:"id"`
	FlightID   int    `json:"flight_id"`
	SeatNumber string `json:"seat_number"`
	Class      string `json:"class"`
	Status     string `json:"status"`
}

type Booking struct {
	ID       int       `json:"id"`
	UserID   int       `json:"user_id"`
	FlightID int       `json:"flight_id"`
	Status   string    `json:"status"`
	BookedAt time.Time `json:"booked_at"`
}

type BookingSeat struct {
	ID        int `json:"id"`
	BookingID int `json:"booking_id"`
	SeatID    int `json:"seat_id"`
}
