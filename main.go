package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq" // PostgreSQL driver

	"github.com/fumetsunoyami/ban_ve_may_bay/handlers"
)

func main() {
	// Kết nối đến cơ sở dữ liệu PostgreSQL (giống như trước)
	connStr := "postgres://postgres:Shadow189@localhost:9999/ban_ve_may_bay?sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Kiểm tra kết nối (giống như trước)
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Kết nối PostgreSQL thành công!")

	// Đăng ký các handler
	http.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request) {
		handlers.RegisterHandler(w, r, db)
	})
	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		handlers.LoginHandler(w, r, db)
	})
	http.HandleFunc("/flights", func(w http.ResponseWriter, r *http.Request) {
		handlers.GetFlightsHandler(w, r, db)
	})
	http.HandleFunc("/bookings", func(w http.ResponseWriter, r *http.Request) {
		handlers.CreateBookingHandler(w, r, db)
	})

	// Khởi động server (giống như trước)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Printf("Server đang chạy tại cổng %s...\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
